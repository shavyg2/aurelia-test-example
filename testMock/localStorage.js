
const storage =[];

class LocalStorage{

  getItem(key){
    return storage[key]|| null;
  }


  setItem(key,value){
    storage[key]=value;
    return true;
  }


  clear(){
    while(storage.length){
      storage.pop();
    }
    return true;
  }

}
module.exports=LocalStorage;
