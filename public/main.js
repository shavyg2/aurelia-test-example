
//Css and Style

import "ratchet/css/ratchet.css!";
import "ratchet/css/ratchet-theme-android.css!";
import "ratchet/js/ratchet";
import "./styles/ui.css!";

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .plugin("./helpers/CustomCssAnimator");

  aurelia.start().then(() => aurelia.setRoot("app/start",document.body));
}
