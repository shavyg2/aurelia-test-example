export default {
  stack:[],

  visible:function(key){
    var stack= this.stack;
    let state = {
      entered:false,
      reenter:false
    };
    var position = stack.indexOf(key);
    state.position=position;
    if(position<0){
      state.entered=true;
    }else{
      state.reenter=true;
    }

    return state;
  },

  enterView:function(key){
    let top=false;
    let {entered,reenter,position}= this.visible(key);
    if(entered){
      this.stack.push(key);
      var r = this.visible(key);
      position=r.position;
      top=true;
    }else{
      this.stack.splice(position+1, Math.pow(2,31));
    }

    return {entered,reenter,position,top};
  }
}
