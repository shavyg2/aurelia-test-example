let EnterAnimationStack=[];
let LeaveAnimationStack=[];

let AnimationVariable;
import * as _md5 from "js-md5";
const md5 =_md5.default;

export class AnimationHelper{

  constructor(){
  }

  extendEnter(animate,func,function_id){
    if(EnterAnimationStack.indexOf(md5(func.toString()))>-1){
      return;
    }
    let _enter= animate.enter;
    //animate["AnimationVariable"] = AnimationVariable;
    EnterAnimationStack.push(md5(func.toString()));
    console.log("Adding to Stack");

    animate.enter = function(){
        return function(element){
          var self=this;
          var r =func.apply(this,arguments);
          if(r && r.then){
            return r.then(function(){
              return _enter.apply(self,arguments);
            })
          }else{
            return _enter.apply(self,arguments);
          }
        }
    }();
  }


  extendLeave(animate,func){
    if(LeaveAnimationStack.indexOf(md5(func.toString()))>-1){
      return;
    }
    let _leave = animate.leave;
    //animate["AnimationVariable"] = AnimationVariable;
    LeaveAnimationStack.push(md5(func.toString()));
    animate.leave = function(){
        return function(element){
          var self=this;
          var r=func.apply(self,arguments);
          if(r && r.then){
            return r.then(function(){
              return _leave.apply(self,arguments);
            })
          }else{
            return _leave.apply(self,arguments);
          }
        }
    }();
  }

}
