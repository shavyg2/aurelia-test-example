import {CssAnimator} from "aurelia-animator-css";
import {animationEvent,TemplatingEngine} from 'aurelia-templating';
import {DOM} from 'aurelia-pal';


export class CustomAnimator extends CssAnimator{


  constructor(){
    console.log("Custom Animation");
    super();
    let stack= this.stack =[]
    stack.contains = function(item){
        return this.indexOf(item) < 0? false:true;
    }

    stack.remove= function(item){
      if(this.contains(item)){
        let index = this.indexOf(item);
        this.splice(index,1);
      }
    }
  }

  enter(element){
    try{let animId = element.id;
    element.animationId=animId;
    if(this.stack.contains(animId)){
      element.classList.add("fromStack");
    }else{
      this.stack.push(animId);
      element.classList.add("toStack");
    }}catch(e){
      console.log(e);
    }
    return super.enter(element);
  }


  leave(element){
    try{let animId = element.id;
    if(this.stack.indexOf(animId)=== this.stack.length-1){
      element.classList.add("outStack");
      this.stack.pop();
    }else{
      element.classList.add("inStack");
    }}catch(e){
      console.log(e);
    }
    return super.leave(element);
  }
}


export function configure(config: Object, callback?:(animator:CssAnimator) => void): void {
  let animator = config.container.get(CustomAnimator);
  config.container.get(TemplatingEngine).configureAnimator(animator);
  if (typeof callback === 'function') { callback(animator); }
}
