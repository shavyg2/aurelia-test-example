export class NavigationRouterClass{
  constructor({router}){
    this.router=router;
  }

  getRoute(name){
    let route= this.router.navigation.filter(r=>r.config.name===name)[0]
    return route && route.href?route.href:"";
  }


  attached(){
    console.log(`attached ${this.element.id}`)
  }

  detached(){
    console.log(`deattached ${this.element.id}`)
  }
}
