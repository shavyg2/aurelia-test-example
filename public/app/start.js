export class Main{

  configureRouter(config,router){
    console.log("Beginning Application");
    config.title="Nutrition2Go";
    config.map([
      {route:['',"tutorial1"],name:"tutorial1",moduleId:"/app/tutorial1",nav:true, title:"Tutorial"},
      {route:["tutorial2"],name:"tutorial2",moduleId:"/app/tutorial2",nav:true, title:"Tutorial"},
      {route:["tutorial3"],name:"tutorial3",moduleId:"/app/tutorial3",nav:true, title:"Tutorial"},
      {route:["setup"],name:"setup",moduleId:"/app/setup",nav:true, title:"Setup"},
      {route:["restrictions"],name:"restrictions",moduleId:"/app/dietary-restrictions",nav:true, title:"Restrictions"},
      {route:["goal"],name:"goal",moduleId:"/app/set-goal",nav:true, title:"Set Goal"},
      {route:["home"],name:"home",moduleId:"/app/home",nav:true, title:"Home"},
      {route:["updateWeight"],name:"updateWeight",moduleId:"/app/update-weight",nav:true, title:"Update Weight"},
      {route:["whatToEat"],name:"whatToEat",moduleId:"/app/what-to-eat",nav:true, title:"What To Eat"}
    ]);
    this.router=router;
  }

  routerLog(router){
    console.info(router);
  }
}
