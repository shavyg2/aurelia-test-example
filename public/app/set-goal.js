//needed for custome animation
import {
  CustomAnimator as CssAnimator
}
from '../helpers/CustomCssAnimator';


//to inject into models
import {
  inject
}
from 'aurelia-framework';


//The routers to configure stuff
import {
  Router
}
from 'aurelia-router';


//parent to grab the parent scope
import {
  Parent
}
from 'aurelia-framework';


//base class to support NavigationRouterClass
//might not be needed
import {
  NavigationRouterClass
}
from "../helpers/NavigationRouterClass.js";

@inject(CssAnimator, Element, Parent.of(Router))
export class SetGoal extends NavigationRouterClass {
  constructor(animate, element, router) {
    super({
      router
    });
    this.animate = animate;
    this.element = element;
    this.router = router;

    this.goal;
    this.goal_weight;

    this.user_goals=[
      "Select a Goal",
      "Lose Weight",
      "Maintain Weight",
      "Gain Weight"
    ];
  }

  done(){
    this.router.navigateToRoute("home", {});
  }

}
