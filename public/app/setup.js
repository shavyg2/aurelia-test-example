//needed for custome animation
import {
  CustomAnimator as CssAnimator
}
from '../helpers/CustomCssAnimator';


//to inject into models
import {
  inject
}
from 'aurelia-framework';


//The routers to configure stuff
import {
  Router
}
from 'aurelia-router';


//parent to grab the parent scope
import {
  Parent
}
from 'aurelia-framework';


//base class to support NavigationRouterClass
//might not be needed
import {
  NavigationRouterClass
}
from "../helpers/NavigationRouterClass.js";

import {User} from "../model/User";

@inject(CssAnimator, Element, Parent.of(Router))
export class Setup extends NavigationRouterClass {
  constructor(animate, element, router) {
    super({
      router
    });
    this.animate = animate;
    this.element = element;
    this.router = router;

    this.name;
    this.age;
    this.weight;
    //this.height-feet eval to this.height subtract feet--- Application crashes
    this.height_feet;
    this.height_inches;
    this.activity_level;

    this.user_activities=[
      "Activity Level",
      "Low",
      "Medium",
      "High"
    ];
  }


  displayRestrictions(){
    this.router.navigateToRoute("restrictions", {});
  }


  done(){

    var user = {name:this.name,age:this.age,height:this.height,weight:this.weight,height_inches:this.height_inches};
    User.Create(user);
    console.log("************************");
    this.router.navigateToRoute("goal", {});
  }

}
