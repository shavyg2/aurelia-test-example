import {BaseModel} from "./Model";

const key = "user_model";

export class User extends BaseModel{

  constructor(options){
    options.key=key;
    super(options);
  }

  save(){
    return super.save(key,this);
  }

  static Create(model){
    return super.Create(key,model);
  }

  static FindById(id){
    return super.FindById(key,id).map(m=>new User(m));
  }


  static FindByName(name){
    return super.FindByText(key,"name",name).map(m=>new User(m));
  }

  static LoadModels(){
    return super.LoadModels(key).map(m=>new User(m));
  }
}



if(typeof window!=="undefined" && window.Nutrition2Go_Development){
  let firstname="Shavauhn";
  let lastname= "Gabay";
  let weight = 260;
  let age = 26;

  let user = User.Create({firstname,lastname,weight,age});
  if(typeof user ==="boolean"){
    console.log(`user created is ${user}`);
  }else{
    console.log(`user is ${JSON.stringify(user)}`);
  }
}
