import {KeyStore} from "./KeyStore";


export class BaseModel extends KeyStore{

  constructor(options) {
    super();
    for(let key in options) {
      switch(key) {
        case "key":
          break;
        default:
          this[key] = options[key];
      }
    }

    if(!!this.id) {
      BaseModel.Create.call(options.key, this);
    }
  }

  save(key) {
    try {
      let models = BaseModel.LoadModels(key)
      let model = models.filter(m => m.id === this.id)[0];
      for(var k in model) {
        model[k] = this[k];
      }
      let text = JSON.stringify(models);
      let store = BaseModel.GetStore();
      store.setItem(key, text);
      return true;
    } catch(e) {
      console.log(e);
      return false;
    }
  }



  static Create(key, model) {
    try {
      const idenifier = key+"_primary_key";
      let id = KeyStore.Key(idenifier)||1;
      model.id=id;
      KeyStore.KeyValue(idenifier,++id);
      let items = BaseModel.LoadModels(key);
      items.push(model);
      var saved=BaseModel.SaveCollection(key,items);
      if(saved===false){
        throw new Error(`${key} was not saved to the database`);
      }
      return model;
    } catch(e) {
      return false;
    }
  }

  static FindById(key, id) {
    let models = BaseModel.LoadModels(key).filter(m => m.id === id);
    return models;
  }

  static FindByText(key, text, value) {
    let models = BaseModel.LoadModels(key).filter(m => m[text] === value);
    return models;
  }

  static SaveCollection(key,collection){
    return super.KeyValue(key,collection);
  }

  static LoadModels(key) {
    let items = super.Key(key)
    if(!items) {
      items = [];
    }
    return items;
  }

  static RemoveItem(key,id){

  }
}
