export class KeyStore {

  static GetStore() {
    return window.localStorage;
  }


  static RemoveDataBase() {
    let store = KeyStore.GetStore();
    store.clear();
  }

  static ClearAll(key) {
    let store = KeyStore.GetStore();
    store.setItem(key, null);
  }


  static KeyValue(key, value) {
    if (typeof value === "object") {
      return KeyStore.KeyValueObject(key, value);
    } else {
      return KeyStore.KeyValueObject(key, value);
    }
  }

  static KeyValueObject(key, value) {
    return KeyStore.KeyValueBasic(key, JSON.stringify(value));
  }

  static KeyValueBasic(key, value) {
    var store = KeyStore.GetStore();
    return store.setItem(key, value);
  }


  static Key(key) {
    var store = KeyStore.GetStore();
    var result;
    try {
      result = KeyStore.KeyObject(key);
    } catch (e) {
      result = store.getItem(key);
    }
    return result;
  }

  static KeyObject(key) {
    var store = KeyStore.GetStore();
    return JSON.parse(store.getItem(key));
  }

}
