import {BaseModel} from "./Model";

const key = "weight_model";


const schema = []

export class Weight extends BaseModel{

  constructor(options){
    options.key=key;
    super(options);
  }

  save(){
    return super.save(key,this);
  }

  static Create(model){
    return super.Create(key,model);
  }

  static FindById(id){
    return super.FindById(key,id);
  }

  static LoadModels(){
    return super.LoadModels(key).map(m=>new Weight(m));
  }
}
