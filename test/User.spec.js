'use strict'

const sinon = require("sinon");
const should = require("chai").should();

var LocalStorage = require("../testMock/localStorage");
let {KeyStore} = require("../public/model/KeyStore");



let {BaseModel} = require("../public/model/Model");

let {User} = require("../public/model/User");


describe("A User",function(){

  before(function(){
    sinon.stub(KeyStore,"GetStore",function(){
      return new LocalStorage();
    });
  })

  it("should be able to load users up",function(){
    var users = User.LoadModels();
    should.exist(users,users);
    users.length.should.equal(0,users.length);
  })

  it("should be able to save to the database",function(){
      var user = User.Create({name:"shavauhn",age:26,weight:260});

      should.exist(user);

      user.should.not.be.a("boolean");

      should.exist(user.id);

      user.id.should.be.a("number");

      user.name.should.equal("shavauhn")
  });

  it("should be able to load users up after the save",function(){
    var users = User.LoadModels();
    should.exist(users);
    users.length.should.equal(1);

    users[0].name.should.equal("shavauhn");
  })

  it("should be able to query a specific user",function(){
      let [shavauhn] = User.FindByName("shavauhn");

      should.exist(shavauhn);
      shavauhn.name.should.equal("shavauhn");

  });



  function GenerateNoiseInDatabase(){
    for(let i=0,len=parseInt(100*Math.random());i<len;i++){
        User.Create({});
    }
  }


  it("should be able to update a user information",function(){
      GenerateNoiseInDatabase();
      const _user = User.Create({name:"Shavauhn Gabay"});
      should.exist(_user);
      var id = _user.id;
      should.exist(id);

      const user = User.FindById(id)[0];
      should.exist(user);

      GenerateNoiseInDatabase();

      user.name.should.equal("Shavauhn Gabay");
      user.id.should.equal(id);
      user.name = "Gregg Jones";
      user.save();



      User.FindById(id)[0];
      user.id.should.equal(id);
      user.name.should.equal("Gregg Jones");
  });


  after(function(){
      KeyStore.GetStore.restore();
  });
})
