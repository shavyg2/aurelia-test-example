'use strict'

const sinon = require("sinon");
const should = require("chai").should();

var LocalStorage = require("../testMock/localStorage");
let {KeyStore} = require("../public/model/KeyStore");





describe("The Key Store Should",function(){

  before(function(){
    sinon.stub(KeyStore,"GetStore",function(){
      return new LocalStorage();
    });
  })

  it("should be able to save a value and retrive it",function(){
    KeyStore.KeyValue("run_once",1);
    let runonce = KeyStore.Key("run_once");
    runonce.should.be.a("number");
  })

  it("should be able to search null values",function(){
    let nothing = KeyStore.Key("nothing");
    should.not.exist(nothing);
  })

  after(function(){
      KeyStore.GetStore.restore();
  });
})
